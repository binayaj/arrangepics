#!/usr/bin/env python
# Author Binaya Joshi
# This code organizes all image and video files in a structured folder (Year/Month) 
# If the file is already present in the destination folder, things such as md5sum are checked and if they
# match, then it is skipped
# This code helps a lot in organizing photos/videos as well as avoid duplicate copies.


import os, time, datetime, hashlib, shutil

def md5(image_file):
    hash_md5 = hashlib.md5()
    with open(image_file, "rb") as file:
        for chunk in iter(lambda: file.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def random_suffix():
    # Delaying so that there is no overlap
    time.sleep(5.0 / 1000.0)
    return time.time()


truth = ['Y', 'Yes','y']
print("Enter the source directory path")
source_path = input()
print("Enter the destination directory path")
dest_path = input()
count=0
duplicates=0

if os.path.exists(dest_path):
    print("DESTINATION FOLDER EXISTS")

if os.path.isdir(source_path):
    print("Process %s " % source_path)
    if input() in truth:
        start=time.time();
        for root, dir, files in os.walk(source_path):
            for file in files:
                if (file.lower().endswith(('.jpg', '.nef', '.jpeg', '.png','.aae','.gif','.dsc', '.mov', 'mp4','.avi'))):

                    src_file = os.path.join(root, file)
                    create_date = time.ctime(os.stat(os.path.join(root, file))[8])
                    month = create_date.split()[1]
                    date = create_date.split()[2]
                    year = create_date.split()[4]
                  #  week = "Week_" + str((int(date) // 7) + 1)
                    if (not file.lower().endswith(('.mov', '.mp4','.avi'))):
                        dest_folder = dest_path + "/" + year + "/" + month + "/"

                    else:
                        dest_folder = dest_path + "/Videos/" + year + "/" + month + "/"
                    dest_file = dest_folder + file

                    # check directory and file existence
                    if not os.path.isdir(dest_folder):
                        os.makedirs(dest_folder)

                    if (not os.path.isfile(dest_file)):
                        print("Moving File :" + dest_file)
                        shutil.move(src_file, dest_file)
                        count+=1
                    else:
                        # If file already exists, calculate MD5Sum and process accordingly.Process for files, whose MD5 differ only.
                        src_md5 = md5(src_file)
                        dest_md5 = md5(dest_file)
                        print ("Busy calculating MD5.....")
                        if (src_md5 != dest_md5):
                            new_name = dest_folder + file.split('.')[-2] + "_" + str(random_suffix()) + "." + \
                                       file.split('.')[-1]
                            shutil.move(src_file, new_name)
                            count+=1
                            print("Renaming and Moving File " + new_name)
                        else:
                            duplicates+=1
        seconds=time.time()-start;
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        print ("Total Time Taken: %d:%02d:%02d" % (h, m, s))


else:
    print("Source folder doesn't exist")

print (str(count) +" files have been moved")
print (str(duplicates) + " files have not been moved")
exit()

