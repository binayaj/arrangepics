## Details

This code organizes all image and video files in a structured folder (Year/Month) format.

If the destination folder already has file with same name, it first checks if their md5sum match. If md5sum matches, then it skips that file as it is a duplicate. If md5sum is different, then it means they are two different files, but with same name. So the program renames the file uniquely and stores in the desired folder.
This code helps a lot in organizing photos/videos avoiding all duplicates

    This program has helped me a lot in saving terrabytes & terrabytes of space in my personal hard drives. More than than, all my pictures are arranged properly and I can locate them easily whenever
     needed (if I have an idea when it is taken). Also, I am confident I am not missing any pictures/videos in my backups as I backup organized folder created by this program.


    Earlier, I had multiple copies of same image files/videos scattered across main and backup drives. I never knew if they were backed up or not. So, fearing, I will loose them, I used to just backup.
    With the help of this program, it is longer the case.  This is how this program was born.


## Usage
```
python arrangepics.py
```

It will then ask you the source folder that contains images/videos to be processed. The source folder can be a nested one too.

Next, it will ask you the destination where arranged images/videos should be stored.

Images/Videos will be moved from your source folder to destination folder in structured tree. Any duplicate image/videos will be left over in the source folder and it can be deleted.



## Arranged Folder Structure

Your image/video files will be arranged in folder structure like this, avoiding any duplicate

![Alt text](https://bitbucket.org/binayaj/arrangepics/raw/95f575c46d84800b3a4d676544b816d3c3806dd9/arrangement.jpg)

## Author
Binaya Joshi
